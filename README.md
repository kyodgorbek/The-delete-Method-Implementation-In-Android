# The-delete-Method-Implementation
@Override
 public int delete(Uri, uri, String where, String[] whereArgs) {
       SQliteDatabase db = mOpenHelper.getWritableDatabase();
       int count;
       switch(SUriMatcher.match(uri)) {
       case INCOMING_BOOK_COLLECTION_URI_INDICATOR:
         count = db.delete(BookTableMetaData.TABLE_NAME, where, whereArgs);
         break;
      
    case_INCOMING_SINGLE_BOOK_URI_INDICATOR:
       String rowId = uri.getPathSegments().get(1);
       count = db.delete(BookTableMetaData.TABLE_NAME)
        , BookTableMetaData._ID + "=" + rowId
        + (!TextUtils.isEmpty(where) ? " And (" + where + ')' : + uri);
      }
      getContext().getContentResolver().notifyChange(uri, null);
      return count;
  }
